### gitlab-cicd
`GitLab CIでテスト・ビルド・デプロイを自動化する`

一、事前準備

1.1. gitlab環境：GitLab(DSLX)利用

1.2. 開発PC：`docker`と`gitlab-runner`がインストール必要

1.3. テストコード：testgolangサンプル用意済

1.4. Dockerfile：ベースとするDockerイメージに対して実行する内容を記述しDockerイメージを生成する

1.5. .gitlab-ci.yml ：ファイルにパイプラインの構造と実行順序を定義する

二、環境設定

2.1. GitLabの画面の「Settings」→「CI/CD」→「Runnsers　settings」から トークン確認

2.2. Runnerを`登録`

三、コードをgitにpushしホスト機に自動ディプロイ、テストURL：http://127.0.0.1:8000

3.1. git pushで masterブランチを更新

3.2. パイプラインが自動で走り出す

3.3 動作確認


### 一、事前準備

1. gitlab環境（テストのためofficialのGitlabリポジトリを利用したが、皆さんの場合GitLab-DSLXがお勧め）
>Gitlabでテスト用のプロジェクトを作成
>>https://docs.gitlab.com/ee/gitlab-basics/create-project.html

2. `docker`と`gitlab-runner`をホスト機（開発PC）にインストール
> dockerインストール方法:
>> https://docs.docker.com/get-docker/

> gitlab-runnerインストール方法:
>> https://docs.gitlab.com/runner/install/
![img](./imgs/WX20200623-095019.png)
※開発PCのOSに応じてクリックする
※上記はインストールまで、runner登録は環境設定部分で実施する

3. `Main.go`テストコード、ここでの開発言語はGolangだが、他の言語も同じ流れになる
![img](./imgs/WX20200623-094232.png)


4. `Dockerfile`ファイル
```
# ベースイメージ
FROM golang:latest
# メンテナンス者
MAINTAINER Kyoku "g.qu@ntt.com"

# イメージ内の镜像中项目路径
WORKDIR $GOPATH/src/kyoku.com/cicd-demo
# 本リポジトリのテストコードをイメージにコピー
COPY main.go $GOPATH/src/kyoku.com/cicd-demo
# イメージ作成
RUN go build .

# コンテナが接続用にリッスンするポートを指定
EXPOSE 8000

# エントリポイント指定
ENTRYPOINT ["./cicd-demo"]
```

5. `.gitlab-ci.yml`ファイル
```
stages:
  - deploy

docker-deploy:
  stage: deploy
  # ジョブ実行内容
  script:
    # Dockerfileからcicd-demoイメージ生成
    - docker build -t cicd-demo .
    # すでに動いているコンテナーがある場合削除
    - if [ $(docker ps -aq --filter "name=cicd-demo") ]; then docker rm -f cicd-demo;fi
    # イメージからコンテナーを起動、 ホストの8000番ポートをコンテナの8000番ポートへマッピング
    - docker run -d -p 8000:8000 --name cicd-demo cicd-demo
  tags:
    # Jobを実行するホスト指定
    - local-runner
  only:
    # masterブランチが更新される場合のみ実行
    - master
```
> tagsのところ必ずrunner登録時入力したtags名を変更してください。

### 二、環境設定

1. Token確認
> GitLabの「Settings」→「CI/CD」→「Runners」
図のtokenは後ほどrunner登録時使われる
![img](./imgs/![img](./imgs/WX20200623-102240.png).png)

2. Runner登録
> https://docs.gitlab.com/runner/register/index.html
![img](./imgs/WX20200623-095125.png)

3. 登録確認
下記の図では、`local-runner`というタグでrunnerを登録しました。
![img](./imgs/WX20200623-102240.png) 

### 三、更新をpushし自動ディプロイ
runner登録後、git pushでMasterブランチが更新されるたび、ジョブが自動で実行される。
> ジョブの実行結果は「CI/CD」の「Pipelines/Jobs」のところで確認する
![img](./imgs/WX20200623-102240.png)

### 四、動作確認
最後にブラウザーから `http://127.0.0.1:8000` にアクセス確認
![img](./imgs/WX20200623-100922.png)



```
予備知識：
    dockerコマンド
    gitコマンド
    gitlabの使用方法
    .gitlab-ci.ymlの編集
    どれかの一つの開発言語、本例はgolang使用
```
